var dotdotdotOptions = {
  truncate: "letter",
  watch: "window",
};

jQuery(document).mouseup(function (e) {
  var container = jQuery('#fixed-main-sidebar-menu');
  // if the target of the click isn't the container nor a descendant of the container
  if (!container.is(e.target) && container.has(e.target).length === 0) {
    toggleFixedSidebar('close');
  }
});

var resetTimer;
jQuery(window).on('resize', function () {
  clearTimeout(resetTimer);
  resetTimer = setTimeout(function () {
    zyxpro_world_map(1);
    zyxpro_hello_location_equal_height();
    jQuery('.trim-zyxpro-content').dotdotdot(dotdotdotOptions);
  }, 250);
});

jQuery(window).scroll(function () {
  init_sticky_header();
});

jQuery(window).on('load', function () {
  zyxpro_hello_location_equal_height();
});

jQuery(document).ready(function () {
  jQuery('.region--navigation-collapsible').niceScroll({
    cursorcolor: "#fff",
    cursorwidth: "4px",
    background: "rgba(0,0,0,0.3)",
    cursorborder: "0px solid transparent",
    cursorborderradius: 0,
    touchemulate: true,
    emulatetouch: true,
  });
  var flag = 0;
  jQuery('.fixed-main-sidebar-menu nav > ul > li').each(function (i, el) {
    if (jQuery(this).find('ul').length > 0) {
      jQuery(this).addClass('has-child').find('> a').after('<span class="menu-child-toggler" />');
      if (flag == 0) {
        jQuery(this).addClass('active');
        flag++;
      }
    }
  });
  toggle_sidebar_menu();
  jQuery('video').attr({
    'data-object-fit': 'cover',
    'data-object-position': 'center center'
  });
  objectFitPolyfill();
  zyxpro_progressive_toggle();
  // Initialize the rwdImageMaps.
  jQuery('img[usemap]').rwdImageMaps();
  zyxpro_footer_location_toggler();
  zyxpro_process_contact_form_field();
  jQuery('.trim-zyxpro-content').dotdotdot(dotdotdotOptions);
  jQuery('.block--type-our-team-block .progressive-loader').on('click', function(e){
    e.preventDefault();
    var parent = jQuery(this).parents('.field--field-member');    
    parent.toggleClass('active');
    if (parent.hasClass('active')) {
      jQuery(this).text(jQuery(this).data('hide-text'));
    } else {
      jQuery(this).text(jQuery(this).data('show-text'));
      jQuery('html, body').animate({
        scrollTop: parent.parents('.block--type-our-team-block').offset().top - 50
      }, 1000);
    }
  });
});

function toggleFixedSidebar(action) {
  if (typeof action != 'undefined' && action == 'close') {
    jQuery('body').removeClass('fixed-sidebar-open');
  } else {
    jQuery('body').addClass('fixed-sidebar-open');
  }
}

// Function to make the header menu sticky on scroll.
function init_sticky_header() {
  if (jQuery('.main-header').length < 1) return false;
  var header = jQuery('.main-header-parent').position().top;
  var scrollTop = jQuery(window).scrollTop();
  if (scrollTop > header) {
    jQuery('body').addClass('header-sticky');
  } else {
    jQuery('body').removeClass('header-sticky');
  }
}

// Function to animate main sidebar.
function toggle_sidebar_menu() {
  jQuery('.fixed-main-sidebar-menu nav > ul > li.has-child .menu-child-toggler').on('click', function () {
    if (jQuery(this).parent('.has-child').hasClass('active')) return '';
    jQuery('.fixed-main-sidebar-menu nav > ul > li.has-child.active').removeClass('active');
    jQuery(this).parent('.has-child').addClass('active');
    jQuery(window).resize();
  });
}


// Function to add progressive loading.
function zyxpro_progressive_toggle() {
  jQuery('.progressive-loader').on('click', function () {
    var container = jQuery(this).closest('.progressive-content-wrap');
    container.toggleClass('display-all-content');
    if (typeof jQuery(this).data('hide-text') !== 'undefined' && typeof jQuery(this).data('show-text') !== 'undefined') {
      if (container.hasClass('display-all-content')) {
        jQuery(this).text(jQuery(this).data('hide-text'));
      } else {
        jQuery(this).text(jQuery(this).data('show-text'));
      }
    }
  });
}

function zyxpro_footer_location_toggler() {
  var item = jQuery('.block--type-our-locations .paragraph--type--location-item .field--field-location');
  item.on('click', function () {
    var parent = jQuery(this).parents().eq(1);
    if (parent.hasClass('active') && jQuery(window).width() > 991) return false;
    if (jQuery('.block--type-our-locations .field--field-location-items > .active').length > 0) {
      jQuery('.block--type-our-locations .field--field-location-items > .active').removeClass('active');
    }
    parent.addClass('active');
    setTimeout(zyxpro_hello_location_equal_height, 600);
  });
}

// Set equal height to location and say hello block above the footer section.
function zyxpro_hello_location_equal_height() {
  var items = jQuery('.region--content-bottom .block');
  items.css('min-height', 'initial');
  if (jQuery(window).width() < 768) return false;
  var height = jQuery('.region--content-bottom').height();
  items.css('min-height', height);
}

// Checks if form item wrap contains and input, textarea for button and add class.
function zyxpro_process_contact_form_field() {
  jQuery('form.contact-form .form-item').each(function () {
    var $this = jQuery(this);
    if ($this.find('input, textarea, button').length < 1) {
      $this.addClass('zyxpro-noInputWrap');
    }
  });
}
