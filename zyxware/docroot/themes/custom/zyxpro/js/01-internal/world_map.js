
var resizeTimer;
jQuery(document).ready(function () {
  zyxpro_world_map(1);
});

jQuery(window).on('resize', function () {
  jQuery('area.position-locked').removeClass('position-locked');
  clearTimeout(resizeTimer);
  resizeTimer = setTimeout(function () {
    zyxpro_world_map(1);
  }, 250);
});

function zyxpro_world_map(flag) {
  jQuery('.world_map map area').on('mouseenter', function (e) {
    var $this = jQuery(this);
    var $parent = $this.closest('.block--type-our-locations');
    var $item = $this.closest('.block--type-our-locations').find('.field--field-location-items > div');
    var id = $this.attr('id');
    var position = $this.position();
    var left = parseFloat(e.clientX) + 30,
      top = parseFloat(e.clientY);
    
    var flag = !$this.hasClass('position-locked');
    // Add class to the corresponding country wrapper when mouse enter the plotted maps pin.
    if (id == 'zyx_india' && $item.eq(0).length > 0) {
      $item.eq(0).addClass('popup-active');
      if (flag) {
        $item.eq(0).css({
          'left': left,
          'top': top
        });
      }
    } else if (id == 'zyx_usa' && $item.eq(1).length > 0) {
      $item.eq(1).addClass('popup-active');
      if (flag) {
        $item.eq(1).css({
          'left': left,
          'top': top
        });
      }
    } else if (id == 'zyx_australia' && $item.eq(2).length > 0) {
      $item.eq(2).addClass('popup-active');
      if (flag) {
        $item.eq(2).css({
          'left': left,
          'top': top
        });
      }
    }
    $this.addClass('position-locked');
  }).on('mouseout', function (e) {
    // Removes the class to all country wrapper when mouse leaves the plotted maps pin.
    jQuery(this).closest('.block--type-our-locations').find('.field--field-location-items > div.popup-active').removeClass('popup-active');
  }).on('click', function (e) {
    e.preventDefault();
  });
}
