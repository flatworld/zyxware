jQuery('.grid-wrap .progressive-btn').on('click', function(){
  var $parent = jQuery(this).parents('.grid-wrap');
  var $target = $parent.find('.field--field-landing-page-items');
  $target.toggleClass('active-items');
  if (!$target.hasClass('active-items')) {    
    jQuery(this).text(jQuery(this).data('show-text'));
    jQuery('body, html').animate({
      scrollTop : parseFloat($parent.offset().top) - 50
    }, 1000);
  } else {
    jQuery(this).text(jQuery(this).data('hide-text'));
  }
  
});