jQuery(document).mouseup(function (e) {
  var container = jQuery('.header-search-wrap.search-open');
  // if the target of the click isn't the container nor a descendant of the container
  if (!container.is(e.target) && container.has(e.target).length === 0) {
    jQuery('.header-search-wrap.search-open').removeClass('search-open');
  }
});
jQuery('.toggle-search-header').click(function () {
  jQuery(this).parent().toggleClass('search-open');
});
