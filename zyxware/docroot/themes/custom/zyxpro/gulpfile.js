(function () {
  'use strict';

  var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    sassGlob = require('gulp-sass-glob'),
    concat = require('gulp-concat'),
    jsFilesPath = [
      './node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
      './js/**/*.js',
      './components/**/*.js'
    ];

  gulp.task('sass', function () {
    return gulp
      .src('./components/style.scss')
      .pipe(sourcemaps.init())
      .pipe(sassGlob())
      .pipe(sass({outputStyle: 'uncompressed'}).on('error', sass.logError))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('./dist/css'));
  });

  gulp.task('script', function () {
    return gulp.src(jsFilesPath)
      .pipe(concat('all.js'))
      .pipe(gulp.dest('./dist/js/'));
  });

  gulp.task('sass:watch', function () {
    gulp.start('sass');
    gulp.watch('./components/**/*.scss', ['sass']);
  });

  gulp.task('script:watch', function () {
    gulp.start('script');
    gulp.watch(jsFilesPath, ['script']);
  });

})();
