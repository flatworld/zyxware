var timer = null;
var zyx_dotdotOption = {
  watch: true
};
jQuery(document).ready(function ($) {
  jQuery('.tabs').tabs();
  // Initialize the rwdImageMaps.
  jQuery('img[usemap]').rwdImageMaps();
  footer_div_same_height();
  // jQuery('.getin-touch-btn').on('click', function (e) {
  //   e.preventDefault();
  //   jQuery('html,body').animate({
  //     scrollTop: parseFloat(jQuery('.region--footer-first').position().top) - 50
  //   }, 1000);
  // });
  jQuery('.carousel.carousel-slider').carousel({
    fullWidth: true,
    indicators: true,
    dragged: false,
    onCycleTo: function () {
      if (timer) {
        clearTimeout(timer); //cancel the previous timer.
        timer = null;
      }
      timer = setTimeout(autoplay, 15000);
    }
  });
  timer = setTimeout(autoplay, 15000);
  jQuery('.stewards_slickSlide .stewards_slickSlide-inner').each(function () {
    var slick = jQuery(this);
    slick.slick({
      dots: false,
      infinite: true,
      speed: 3000,
      slidesToShow: 5,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000,
      prevArrow: '<a class="left slide-control" ><img src="/themes/custom/zyxpro_light/images/icons/arrow-left-black.png"></a>',
      nextArrow: '<a class="right slide-control"><img src="/themes/custom/zyxpro_light/images/icons/arrow-right-black.png"></a>',
      responsive: [
        {
          breakpoint: 1366,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 750,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 350,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        }
      ]
    });
  });
});

jQuery(function(){
  jQuery('#edit-submit').on('click',function(){
    var name = jQuery('#edit-name').val();
    name = name.trim();
    var email = jQuery('#edit-mail').val();
    var fname = "";
    var lname = "";
    if(name != "" && email != ""){
      if(name.indexOf(' ') >= 0){
        fname = name.substr(0, name.indexOf(' '));
        lname = name.substr(name.indexOf(' '), name.length);
      }else{
        fname = name;
        lname = name
      }
      jQuery.post( "https://crm.na1.insightly.com/WebToLead/Create", { formId:"MTQi370cQFwNGiKszWJ5hw==", ResponsibleUser: "1841561", LeadSource:"742190", FirstName: fname, LastName: lname, email:email, OrganizationName: "" } );      
      console.log("Sent");
    }
  });
});

jQuery(window).on('scroll', function () {
  // if (jQuery('body').hasClass('fixed-sidebar-open')) {
  //   jQuery('body').removeClass('fixed-sidebar-open');
  // }
});
if (jQuery('header.region-header .sticky-head-wrap .region--header nav > ul > li:has(ul)')) {
  jQuery('<span class="frame"><i class="fa fa-caret-down"></i></span>').insertAfter('header.region-header .sticky-head-wrap .region--header nav > ul > li:has(ul) > a');
  // if(jQuery(window).width() < 992){
  jQuery('header.region-header .sticky-head-wrap .region--header nav > ul > li:has(ul) > a').addClass("link");
  // }
  jQuery('header.region-header .sticky-head-wrap .region--header nav > ul > li > .frame').on('click', function () {
    jQuery(this).find('i').toggleClass('fa-caret-down fa-caret-up');
    jQuery(this).parent().toggleClass('sub-active');
  });
}
var resetTimer;
/*
jQuery(window).on('resize', function () {
  clearTimeout(resetTimer);
  resetTimer = setTimeout(function () {
    if (typeof footer_div_same_height == "function") {
      footer_div_same_height();
    }
    if (typeof overview_block_equal_height == "function") {
      overview_block_equal_height();
    }
    if (typeof testimonial_block_same_height == "function") {
      testimonial_block_same_height();
    }
    jQuery(".insight-content .insight-body, .insight-body .body, .insight-title, .trim_zyx_content").dotdotdot(zyx_dotdotOption);
  }, 250);
});
*/

function toggleFixedSidebar() {
  jQuery('body').toggleClass('fixed-sidebar-open');
}

/*
jQuery(window).on('load', function () {
  footer_div_same_height();
  jQuery(".insight-content .insight-body, .insight-body .body, .insight-title, .trim_zyx_content").dotdotdot(zyx_dotdotOption);
});
*/

function footer_div_same_height() {
  var larger = 0;
  jQuery('.region--footer > *').css('height', 'auto');
  jQuery('.region--footer > *').each(function () {
    larger = (larger > jQuery(this).height()) ? larger : jQuery(this).height();
  });
  if (jQuery(window).width() < 601) larger = 'auto';
  jQuery('.region--footer > *').css('height', larger);
}

function leftslide($this) {
  jQuery($this).parents('.carousel').carousel('prev');
}

function rightslide($this) {
  jQuery($this).parents('.carousel').carousel('next');
}

function autoplay() {
  jQuery('.carousel').carousel('next');
}
jQuery(".zyxpro-download-casestudy").on('click', function (){
  var divToPrint = '';
  if (jQuery('.region--content').length > 0) {
    divToPrint = jQuery('.region--content').get(0);
  }
  var title = '';
  if (jQuery('.block--id-page-title-block h1').length > 0) {
    title = jQuery('.block--id-page-title-block h1').text().trim();
  }
  var htmlToPrint = '' +
      '<style type="text/css">' +
      'table th, table td {' +
      'border:1px solid #000;' +
      'padding;0.5em;' +
      '}' +
      '</style>';
  htmlToPrint += divToPrint.outerHTML;
  newWin = window.open("");
  newWin.document.write("<h3 align='center'>"+title+"</h3>");
  newWin.document.write(htmlToPrint);
  newWin.print();
  newWin.close();
});

function goBack() {
  window.history.back();
}