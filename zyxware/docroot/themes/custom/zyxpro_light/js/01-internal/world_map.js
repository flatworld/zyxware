var resizeTimer;
jQuery(document).ready(function () {
  zyxpro_world_map(1);
});

jQuery(window).on('resize', function () {
  jQuery('area.position-locked').removeClass('position-locked');
  jQuery('.field--field-location-items > .popup-active').removeClass('popup-active');
});

function zyxpro_world_map(flag) {
  jQuery('.world_map__wrap .map-marker').on('mouseover', function () {
    if (jQuery(this).parent().hasClass('active')) {
      jQuery(this).parent().removeClass('active');
    } else {
      jQuery('.world_map__wrap .location--item-wrap.active').removeClass('active');
      jQuery(this).parent().addClass('active');
    }
  });
  jQuery(document).on('mouseover',function (e) {
    var $target = jQuery(e.target);
    if (!($target.hasClass('img-marker') || $target.parents('.map-content').length)) {
      jQuery('.world_map__wrap .location--item-wrap.active').removeClass('active');
    }
  });
}
