jQuery(document).ready(function () {
  jQuery('.carousel-slider .zyxProLight_slickSlide-inner').each(function () {
    var slick = jQuery(this);
    var arrows = false;
    var dots = true;
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      dots = false;
      arrows = true;
    }console.log(dots);console.log(arrows);
    slick.slick({
      arrows: arrows,
      dots: dots,
      prevArrow: '<a class="left slide-control" ><img src="/themes/custom/zyxpro_light/images/icons/arrow-left-black.png"></a>',
      nextArrow: '<a class="right slide-control" onclick="rightslide(this)" ><img src="/themes/custom/zyxpro_light/images/icons/arrow-right-black.png"></a>',
      adaptiveHeight: true,
    });
  });
  jQuery('.testimonial-slider .zyxProLight_slickSlide-inner').each(function () {
    var slick = jQuery(this);
    var arrows = slick.attr('data-arrows') ? slick.data('arrows') : true;
    var dots = slick.attr('data-dots') ? slick.data('dots') : false;
    slick.slick({
      arrows: arrows,
      dots: dots,
      prevArrow: '<a class="left slide-control" ><img src="/themes/custom/zyxpro_light/images/icons/arrow-left.png"></a>',
      nextArrow: '<a class="right slide-control" onclick="rightslide(this)" ><img src="/themes/custom/zyxpro_light/images/icons/arrow-right.png"></a>',
      adaptiveHeight: true,
    });
  });
  testimonial_block_same_height();
});


function testimonial_block_same_height() {
  // jQuery('.zyxProLight_slickSlide .zyxProLight_slickSlide-inner .testimonial > *').css('height', 'auto');
  // jQuery('.zyxProLight_slickSlide .zyxProLight_slickSlide-inner .carousel-item').each(function () {
  //   jQuery(this).find('.testimonial > *').css('height', jQuery(this).find('.testimonial').height());
  // });
  jQuery('.zyxProLight_slickSlide .zyxProLight_slickSlide-inner .testimonial > *').css('height', 'auto');
  var people_details_height = jQuery('.person-details').outerHeight();
  var testimonial_quote_height = jQuery('.testimonial_quote').outerHeight();
  var highest = 0;
  if(jQuery('.person-details').outerHeight() > jQuery('.testimonial_quote').outerHeight()) {
    jQuery('.testimonial > *').css('height', jQuery('.person-details').outerHeight());
    highest = people_details_height;
  }
  else {
    jQuery('.testimonial > *').css('height', jQuery('.testimonial_quote').outerHeight());
    highest = testimonial_quote_height;
  }
  jQuery('#zyxTestimonial').css('height',highest);
  jQuery('#zyxTestimonial > *').css('height',highest);
  jQuery('.slick-list.draggable').css('height',highest);
  jQuery('.slick-list.draggable > *').css('height',highest);
}
