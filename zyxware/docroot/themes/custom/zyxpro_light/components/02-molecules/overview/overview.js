function overview_block_equal_height() {
  jQuery('.overview-block').each(function () {
    jQuery(this).find('.overview-image').css('min-height', 'initial');
    jQuery(this).find('.overview-content').css('min-height', 'initial');
    if (jQuery(window).width() > 992) {
      var height = jQuery(this).height();
      jQuery(this).find('.overview-image').css('min-height', height);
      jQuery(this).find('.overview-content').css('min-height', height);
    }
  });
}


jQuery(document).ready(overview_block_equal_height);
jQuery(window).on('resize', function() {
  overview_block_equal_height();
})