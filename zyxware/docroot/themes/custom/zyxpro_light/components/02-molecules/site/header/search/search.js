jQuery(document).ready(function () {
  // Open up the serch box if it has a value in initial state.
  if (jQuery('header .header-sitewide-search input.form-text').val() != '') {
    jQuery('header .header-sitewide-search form').addClass('serach-form-active');
  }
});

/*
 * Add the class for the icon to text field behaviuor.
 */
jQuery('header .header-sitewide-search input.form-text').on('focus', function (e) {
  var form = jQuery(this).parents('form');
  if (form.hasClass('serach-form-active')) return;
  form.addClass('in');
  setTimeout(function () {
    form.addClass('serach-form-active');
    form.removeClass('in');
  }, 1300);
}).blur(function () {
  if (jQuery(this).val() == '') {
    jQuery(this).parents('form').removeClass('serach-form-active');
  }
});

/*
 * Convert the text field to icon on clicking close icon.
 */
jQuery('header .header-sitewide-search .search_input_after').on('click', function (e) {
  e.preventDefault();
  var form = jQuery(this).parents('form');
  if (!form.hasClass('serach-form-active')) return;
  form.find('input.form-text').val('');
  form.addClass('close');
  form.removeClass('serach-form-active');
  setTimeout(function () {
    form.removeClass('close');
  }, 1300);
})
