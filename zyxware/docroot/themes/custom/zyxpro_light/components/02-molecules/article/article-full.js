jQuery(document).ready(function ($) {

  // All paragraphs in article body
  var paragraphs = jQuery('.content .article-body p');

  // Add divs where after ads will be displayed to adPositions
  // Push div to this array if you want to display ad after that
  var adPositions = [paragraphs[0]];
  
  // Ad div content
  var adDiv = '<div class="zyx-advt-wrap"></div>';

  // Appending ad after adPositions
  jQuery(adPositions).each(function() {
    jQuery(this).after(adDiv);
  });
});