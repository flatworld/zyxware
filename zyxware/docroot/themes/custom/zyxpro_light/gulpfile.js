(function () {
  'use strict';

  var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    sassGlob = require('gulp-sass-glob'),
    concat = require('gulp-concat'),
    jsFilesPath = [
      './js/**/*.js',
      './components/**/*.js'
    ];

  // Gulp task to compile the changes in Sass files.
  gulp.task('sass', function () {
    return gulp
      .src('./components/style.scss')
      .pipe(sourcemaps.init())
      .pipe(sassGlob())
      .pipe(sass({
        outputStyle: 'uncompressed'
      }).on('error', sass.logError))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('./dist/css'));
  });

  // Gulp task to compile the changes in JS files.
  gulp.task('script', function () {
    return gulp.src(jsFilesPath)
      .pipe(concat('all.js'))
      .pipe(gulp.dest('./dist/js/'));
  });

  // Gulp task to watch and compile the changes in SASS files.
  gulp.task('sass:watch', function () {
    gulp.watch('./components/**/*.scss').on('change', gulp.series('sass'));
  });

  // Gulp task to watch and compile the changes in JS files.
  gulp.task('script:watch', function () {
    gulp.watch(jsFilesPath).on('change', gulp.series('script'));
  });

})();
