<?php
/**
 * @file
 * Contains Drupal\add_tags\Controller\addTagsController.
 */
namespace Drupal\add_tags\Controller;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

/**
 * Provides route responses for the Example module.
 */
class addTagsController
{
  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function addTags()
  {

    $filename = 'modules/custom/add_tags/files/tags.csv';

    chmod($filename, 0777);
    // The nested array to hold all the arrays
    $articles = [];
    // Open the file for reading
    if (($h = fopen("{$filename}", "r")) !== false)
    {
      // Each line in the file is converted into an individual array that we call $data
      // The items of the array are comma separated
      while (($data = fgetcsv($h, 1000, ",")) !== false)
      {
        // Each individual array is being pushed into the nested array
        $articles[] = $data;
      }

      // Close the file
      fclose($h);
    }

    foreach ($articles as $key => $item)
    {
      if ($key != 0)
      {
        $base_path = \Drupal::request()->getSchemeAndHttpHost();
        $path_alias = str_replace($base_path, '', $item[1]);
        $path = \Drupal::service('path.alias_manager')->getPathByAlias($path_alias);
        if (preg_match('/node\/(\d+)/', $path, $matches))
        {
          $node = \Drupal\node\Entity\Node::load($matches[1]);
        }
        // $nid = intval($item[2]);
        $tags = [];
        if (strlen($item[8]) != 0)
        {
          $tags = explode(',', $item[8]);
        }
        // var_dump(count($tags)); continue;
        $terms = [];
        // Loading node
        if (count($tags) != 0)
        {
          // $node = Node::load($nid);
          if ($node != NULL)
          {
            // Looping through tags
            foreach ($tags as $tag)
            {
              if ($item[3] == 'Article')
              {
                // Check if the taxonomy already exists
                $query = \Drupal::entityQuery('taxonomy_term');
                $query->condition('vid', "tags");
                $query->condition('name', $tag);
                $tids = $query->execute();

                if (empty($tids))
                {
                  // Creating new taxonomy term
                  $term = Term::create(['vid' => 'tags', 'name' => $tag, ]);

                  $term->save();
                  $tid = intval($term->id());

                  // Pushing to terms array
                  // with tid as index
                  $terms[$tid] = $tid;
                }
                else
                {
                  // Changing string values to int
                  $tids = array_map(function ($value)
                  {
                    return intval($value);
                  }
                  , $tids);

                  // Pushing to terms array
                  // with tid as index
                  foreach ($tids as $tid)
                  {
                    $terms[$tid] = $tid;
                  }
                }
                // Saving taxonomy terms to the node
                $node->field_tags = NULL;
                foreach ($terms as $term)
                {
                  $node->field_tags[] = $term;
                }
              }
              else
              {
                // Check if the taxonomy already exists
                $query = \Drupal::entityQuery('taxonomy_term');
                $query->condition('vid', "vocabulary_2");
                $query->condition('name', $tag);
                $tids = $query->execute();

                if (empty($tids))
                {
                  // Creating new taxonomy term
                  $term = Term::create(['vid' => 'vocabulary_2', 'name' => $tag, ]);

                  $term->save();
                  $tid = intval($term->id());

                  // Pushing to terms array
                  // with tid as index
                  $terms[$tid] = $tid;
                }
                else
                {
                  // Changing string values to int
                  $tids = array_map(function ($value)
                  {
                    return intval($value);
                  }
                  , $tids);

                  // Pushing to terms array
                  // with tid as index
                  foreach ($tids as $tid)
                  {
                    $terms[$tid] = $tid;
                  }
                }
                // Saving taxonomy terms to the node
                $node->taxonomy_vocabulary_2 = NULL;
                foreach ($terms as $term)
                {
                  $node->taxonomy_vocabulary_2[] = $term;
                }
              }
            }
            // Saving node
            $node->save();
          }
        }
      }
      // exit;
    }
    $total_upload_count = count($articles) - 1;
    $element = array(
      '#markup' => '<h3 align="center"><b>Tags for ' . $total_upload_count . ' articles added successfully.</b></h3>',
    );
    return $element;
  }
}
?>
