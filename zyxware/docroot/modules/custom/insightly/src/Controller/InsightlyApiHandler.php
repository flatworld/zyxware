<?php

namespace Drupal\insightly\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller for DBTNG Example.
 */
class InsightlyApiHandler extends ControllerBase {

  /**
   * Render a list of entries in the database.
   */
  public function insightlyMapFields($data) {
    $config = \Drupal::service('config.factory')->getEditable('insightly.settings');
    $accepted_fields = $config->get('insightly_accepted_fields');
    $result = insightly_get_field_mapping($data['form_id']);
    $mapping_id = $result['id'];
    $form_id = $result['form_id'];
    $mapping_details = json_decode($result['mapping']);
    $contact_details = array(
      'FIRST_NAME' => 'To be filled',
      'LAST_NAME' => 'To be filled',
      'BACKGROUND' => 'To be filled',
      'IMAGE_URL' => 'To be filled',
      'EMAIL' => 'To be filled',
      'PHONE' => 'To be filled',
      'BID_AMOUNT' => 0,
    );
    $fields = array();
    $fields = $data['values'];
    foreach ($mapping_details as $form_field => $insightly_field) {
      /*
       * The built in fields have direct values and custom fields
       * are in array format. So, treat them differently.
       */

      // For built in fields such as name, email and copy.
      if (in_array($form_field, array('name', 'mail', 'copy'))) {
        $field_values = ($fields[$form_field]) ? $fields[$form_field] : 'To be filled';
        $contact_details[$insightly_field] = $field_values;
      }
      // For custom fields of the form 'filed_ ...' and fields other than
      // name, copy and mail.
      else {
        $val = $fields[$form_field][0]['value'];
        $field_values = ($val) ? $val : 'To be filled';
        $contact_details[$insightly_field] = $field_values;
      }
    }
    $custom_fields = array_diff_key($contact_details, array_flip($accepted_fields));
    $custom_field_string = '';
    foreach ($custom_fields as $custom_insightly_field => $form_field) {
      $form_field = ($form_field) ? $form_field : 'To be filled';
      $custom_field_string .= '{"CUSTOM_FIELD_ID": "' . $custom_insightly_field;
      $custom_field_string .= '", "FIELD_VALUE": "' . $form_field . '"}, ';
    }
    $custom_field_string = substr_replace(rtrim($custom_field_string), "", -1);
    $contact_details['custom_field_string'] = $custom_field_string;
    $this->insightlyIntegration($contact_details);
  }

  /**
   * Custom function used to send and retrive data from Insightly.
   *
   * @param array $contact_details
   *   Array of contact details from the forms.
   */
  public function insightlyIntegration($contact_details) {
    date_default_timezone_set('UTC');
    $date = date('Y-m-d H:m:s');
    // Checking whether the contact already exists in Insightly.
    // url type for v2.2.
    $url = 'https://api.insight.ly/v2.2/Contacts/Search?email=';
    $url .= $contact_details['EMAIL'];
    $url .= '&brief=false&count_total=false';
    $method = 'GET';
    $type = 'contact';
    $contact_data = $this->insightlyTransferData($type, $url, $method);
    $contact_id = $contact_data['contact_id'];
    $owner_id = $contact_data['owner_id'];
    // Create a new contact if contact not exist in insightly.
    $email_contact = '{"CONTACT_INFO_ID":1,"TYPE":"EMAIL","SUBTYPE":"Work","LABEL":null,"DETAIL":"' . $contact_details['EMAIL'] . '"}';
    $phone_contact = '{"CONTACT_INFO_ID":2,"TYPE":"PHONE","SUBTYPE":"Work","LABEL":null,"DETAIL":"' . $contact_details['PHONE'] . '"}';
    if (!$contact_id) {
      $url = 'https://api.insight.ly/v2.2/Contacts';
      $method = 'POST';
      $contact = '{"CONTACT_ID": 0,
        "SALUTATION": null,
        "FIRST_NAME": "' . $contact_details['FIRST_NAME'] . '",
        "LAST_NAME": "' . $contact_details['LAST_NAME'] . '",
        "BACKGROUND": "' . $contact_details['BACKGROUND'] . '",
        "IMAGE_URL": "' . $contact_details['IMAGE_URL'] . '",
        "DEFAULT_LINKED_ORGANISATION": null,
        "DATE_CREATED_UTC": "' . $date . '",
        "DATE_UPDATED_UTC": "' . $date . '",
        "VISIBLE_TO": "EVERYONE",
        "VISIBLE_TEAM_ID": null,
        "VISIBLE_USER_IDS": null,
        "CUSTOMFIELDS": [' . $contact_details['custom_field_string'] . '],
        "ADDRESSES": [],
        "CONTACTINFOS": [' . $email_contact . ', ' . $phone_contact . '],
        "DATES": [],
        "TAGS": [],
        "LINKS": [],
        "CONTACTLINKS": [],
        "EMAILLINKS": null
        }';
      $contact_id  = $this->insightlyTransferData($type, $url, $method, $contact);
    }
    // Create Leads against a contact submission.
    $url = 'https://api.insight.ly/v2.2/Leads';
    $method = 'POST';
    $type = 'leads';
    $leads = '{
      "LEAD_ID": 0,
      "SALUTATION": null,
      "TITLE": null,
      "FIRST_NAME": "' . $contact_details['FIRST_NAME'] . '",
      "LAST_NAME": "' . $contact_details['LAST_NAME'] . '",
      "ORGANIZATION_NAME": null,
      "PHONE_NUMBER": null,
      "MOBILE_PHONE_NUMBER": null,
      "FAX_NUMBER": null,
      "EMAIL_ADDRESS": "' . $contact_details['EMAIL'] . '",
      "WEBSITE_URL": null,
      "OWNER_USER_ID": "' . $owner_id . '",
      "DATE_CREATED_UTC": "' . $date . '",
      "DATE_UPDATED_UTC": "' . $date . '",
      "CONVERTED": true,
      "CONVERTED_DATE_UTC": null,
      "CONVERTED_CONTACT_ID": null,
      "CONVERTED_ORGANIZATION_ID": null,
      "CONVERTED_OPPORTUNITY_ID": null,
      "VISIBLE_TO": null,
      "RESPONSIBLE_USER_ID": "' . $owner_id . '",
      "INDUSTRY": null,
      "LEAD_STATUS_ID": null,
      "LEAD_SOURCE_ID": null,
      "VISIBLE_TEAM_ID": null,
      "EMPLOYEE_COUNT": null,
      "LEAD_RATING": 1,
      "LEAD_DESCRIPTION": "' . $contact_details['BACKGROUND'] . '",
      "VISIBLE_USER_IDS": null,
      "ADDRESS_STREET": null,
      "ADDRESS_CITY": null,
      "ADDRESS_STATE": null,
      "ADDRESS_POSTCODE": null,
      "ADDRESS_COUNTRY": null,
      "TAGS": [],
      "EVENT_LINKS": [],
      "TASK_LINKS": [],
      "NOTE_LINKS": [],
      "FILE_ATTACHMENTS": [],
      "EMAIL_LINKS": null,
      "IMAGE_URL": null
    }';
    $this->insightlyTransferData($type, $url, $method, $leads);
  }

  /**
   * Custom function to transfer data from and to Insightly.
   *
   * @param string $url
   *   The url responsible for the action.
   * @param string $method
   *   Values will be GET or POST.
   * @param array $data
   *   Json array to create contacts or opportunity.
   */
  public function insightlyTransferData($type, $url, $method, $data = NULL) {
    $ch = curl_init($url);

    $config = \Drupal::service('config.factory')->getEditable('insightly.settings');
    $api_key = $config->get('insightly_api_key');
    $user = base64_encode($api_key);
    curl_setopt($ch,
      CURLOPT_HTTPHEADER,
      array('Content-Type: application/json',
        'Authorization: Basic ' . $user,
      ));
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    if ($method == 'GET') {
      curl_setopt($ch, CURLOPT_POST, 0);
    }
    elseif ($method == 'POST') {
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    $output = curl_exec($ch);
    $result = json_decode($output, TRUE);
    curl_error($ch);
    curl_close($ch);
    $return_data = array();
    if ($type == 'contact') {
      if ($method == 'GET') {
        $return_data['contact_id'] = $result[0]['CONTACT_ID'];
        $return_data['owner_id'] = $result[0]['OWNER_USER_ID'];
      }
      elseif ($method == 'POST') {
        $return_data['contact_id'] = $result['CONTACT_ID'];
        $return_data['owner_id'] = $result['OWNER_USER_ID'];
      }
      return $return_data;
    }
  }

}
