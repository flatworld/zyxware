<?php

namespace Drupal\insightly\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements the SimpleForm form controller.
 *
 * This example demonstrates a simple form with a singe text input element. We
 * extend FormBase which is the simplest form base class used in Drupal.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class ImportForm extends FormBase {

  /**
   * Build the simple form.
   *
   * A build form method constructs an array that defines how markup and
   * other form elements are included in an HTML form.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['insightly_import'] = [
      '#title' => t('Import'),
      '#type' => 'textarea',
      '#default_value' => NULL,
    ];
    $form['insightly_override'] = [
      '#title' => t('Replace an existing field mapping, if one exists with the same Form id'),
      '#type' => 'checkbox',
      '#default_value' => FALSE,
    ];
    $form['insightly_import_submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
    ];
    return $form;
  }

  /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller. It must be
   * unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'insightly_form';
  }

  /**
   * Implements a form submit handler.
   *
   * The submitForm method is the default method called for any submit elements.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /*
     * The insightly_import_form_submit() functionalities in D7 is implemented
     * in this function.
     */
    $insightly_import = $form_state->getValue('insightly_import');
    $insightly_data = explode("|", $insightly_import);
    $form_id = $insightly_data[0];
    $mapping = t($insightly_data[1]);
    $override_check = $form_state->getValue('insightly_override');
    // Get id if the entered form is already mapped.
    $query = \Drupal::database()->select('insightly', 'i');
    $query->fields('i', ['id']);
    $query->condition('form_id', $form_id);
    $results = $query->execute();
    $id = '';
    while ($row = $results->fetchAssoc()) {
      $id = $row['id'];
    }
    if ($id) {
      // Allow override or not for import mapping fields.
      if ($override_check == 1) {
        $update = \Drupal::database()->update('insightly');
        $update->condition('form_id', $form_id);
        $update->fields([
          'mapping' => $mapping,
        ]);
        $update->execute();
        // Redirecting to admin/config/insightly-mapping/add.
        $response = new RedirectResponse(\Drupal::url('insightly.add'));
        $response->send();
      }
      else {
        $message = 'The entered form is already mapped! ';
        $message .= 'Please click on checkbox to override the existing mapping.';
        $messenger = \Drupal::messenger();
        $messenger->addWarning($this->t($message));
      }
    }
    else {
      $insert = \Drupal::database()->insert('insightly');
      $insert->fields([
        'form_id',
        'mapping',
      ]);
      $insert->values(array(
        $form_id,
        $mapping,
      ));
      $insert->execute();
      // Redirecting to admin/config/insightly-mapping/add.
      $response = new RedirectResponse(\Drupal::url('insightly.add'));
      $response->send();
    }
  }

}
