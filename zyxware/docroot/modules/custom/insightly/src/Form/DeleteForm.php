<?php

namespace Drupal\insightly\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Implements the SimpleForm form controller.
 *
 * This example demonstrates a simple form with a singe text input element. We
 * extend FormBase which is the simplest form base class used in Drupal.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class DeleteForm extends FormBase {

  /**
   * Build the Delete form.
   *
   * Confirmation form before deleting mapping.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {

    $form_state->set('insightly_table_form_id', $id);
    $form['insightly_delete'] = [
      '#type' => 'markup',
      '#markup' => '<h3>Are you sure you want to delete this mapping?</h3>',
    ];
    // Generating cancel button link.
    $cancel_url = Url::fromRoute('insightly.add');
    $cancel_link = Link::fromTextAndUrl($this->t('Cancel'), $cancel_url)->toString();
    $form['insightly_delete_submit'] = [
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#description' => t('This action cannot be undone.'),
    ];
    $form['insightly_cancel'] = [
      '#type' => 'markup',
      '#markup' => $cancel_link,
    ];
    return $form;
  }

  /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller. It must be
   * unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'insightly_form';
  }

  /**
   * Implements form validation.
   *
   * The validateForm method is the default method called to validate input on
   * a form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Implements a form submit handler.
   *
   * The submitForm method is the default method called for any submit elements.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /*
     * Perform deleteion in db.
     */
    $id = $form_state->get('insightly_table_form_id');
    $query = \Drupal::database()->delete('insightly');
    $query->condition('id', $id);
    $query->execute();
    // Redirecting to admin/config/insightly-mapping/add.
    $response = new RedirectResponse(\Drupal::url('insightly.add'));
    $response->send();
  }

}
