<?php

namespace Drupal\insightly\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Implements the SimpleForm form controller.
 *
 * This example demonstrates a simple form with a singe text input element. We
 * extend FormBase which is the simplest form base class used in Drupal.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class MappingForm extends FormBase {

  /**
   * Build the Mapping form.
   *
   * A build form method constructs an array that defines how markup and
   * other form elements are included in an HTML form.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $accepted_fields = [
      'FIRST_NAME',
      'LAST_NAME',
      'BACKGROUND',
      'IMAGE_URL',
      'EMAIL',
      'PHONE',
      'BID_AMOUNT',
    ];

    $config = \Drupal::service('config.factory')->getEditable('insightly.settings');
    $config->set('insightly_accepted_fields', $accepted_fields)->save();
    $form['insightly_api_key'] = [
      '#type' => 'textfield',
      '#title' => t('API key'),
      '#default_value' => $config->get('insightly_api_key'),
      '#description' => t('Enter the insightly API key.'),
      '#required' => TRUE,
    ];
    $form['import'] = [
      '#type' => 'markup',
      '#markup' => '<ul class="action-links">
        <li><a href="/admin/insightly-mapping/import">Import</a></li>
        </ul>',
      '#tree' => TRUE,
    ];
    $form['#tree'] = TRUE;
    $header = array('FORM_IDs', 'OPERATIONS', '', '');
    $results = insightly_get_all_field_mapping();
    if ($results == NULL) {
      $rows = NULL;
    }
    else {
      foreach ($results as $result) {

        // Creating edit, delete & export url links.
        $edit_url = Url::fromRoute('insightly.edit', ['id' => $result['id']]);
        $edit_link = Link::fromTextAndUrl($this->t('edit'), $edit_url)->toString();
        $delete_url = Url::fromRoute('insightly.delete', ['id' => $result['id']]);
        $delete_link = Link::fromTextAndUrl($this->t('delete'), $delete_url)->toString();
        $export_url = Url::fromRoute('insightly.export', ['id' => $result['id']]);
        $export_link = Link::fromTextAndUrl($this->t('export'), $export_url)->toString();
        $rows[] = [
          $result['form_id'],
          $edit_link,
          $delete_link,
          $export_link,
        ];
      }
    }
    if ($rows != NULL) {
      $form['insightly_table_title'] = [
        '#type' => 'markup',
        '#markup' => '<h3>Currently Mapped forms</h3>',
        '#tree' => TRUE,
      ];
      $form['mapped_form_table'] = [
        '#type' => 'table',
        '#title' => t('Current Mapped forms'),
        '#header' => $header,
        '#rows' => $rows,
        '#tree' => TRUE,
      ];
    }
    $form['wrapper_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => t('Add New Mapping'),
    ];
    $form['wrapper_fieldset']['insightly_form_id'] = [
      '#type' => 'textfield',
      '#title' => t('Form id'),
    ];
    $field_list_string = '';
    foreach ($accepted_fields as $accepted_field) {
      $field_list_string .= $accepted_field . ', ';
    }
    $field_list_string = substr_replace(trim($field_list_string), "", -1);
    $form['wrapper_fieldset']['insightly_fields_markup'] = [
      '#markup' => t('Available fields in insightly to set target values are :field_list_string All other data field will be added as CUSTOMFIELDS (You need to add custom fields at System Settings > Custom Fields).', array(':field_list_string' => $field_list_string . '.')),
      '#prefix' => '<div class="clear">',
      '#suffix' => '</div>',
    ];
    $form['wrapper_fieldset']['insightly_mapping_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => t('Mapping'),
      // Set up the wrapper so that AJAX will be able to replace the fieldset.
      '#prefix' => '<div id="insightly-fieldset-wrapper" class="row">',
      '#suffix' => '</div>',
    ];
    // Build the fieldset with the proper number of mappings. We'll use
    // $form_state['num_fields'] to determine the number of textfields to build.
    $num_fields = $form_state->get('num_fields');
    if (empty($num_fields)) {
      $form_state->set('num_fields', 1);
    }
    $num_fields = $form_state->get('num_fields');
    for ($i = 0; $i < $num_fields; $i++) {
      $form['wrapper_fieldset']['insightly_mapping_fieldset'][$i]['insightly_source'] = [
        '#type' => 'textfield',
        '#title' => t('Source'),
        '#prefix' => '<div class="col-source">',
        '#suffix' => '</div>',
      ];
      $form['wrapper_fieldset']['insightly_mapping_fieldset'][$i]['insightly_target'] = [
        '#type' => 'textfield',
        '#title' => t('Target'),
        '#prefix' => '<div class="col-target">',
        '#suffix' => '</div>',
      ];
      $form['wrapper_fieldset']['insightly_mapping_fieldset'][$i]['insightly_remove'] = [
        '#title' => t('Remove'),
        '#type' => 'checkbox',
        '#default_value' => 0,
        '#prefix' => '<div class="col-remove">',
        '#suffix' => '</div>',
      ];
    }
    $form['insightly_add_field'] = [
      '#type' => 'submit',
      '#value' => t('Add Field'),
      '#submit' => ['::insightlyAddMoreFields'],
      // See the examples in ajax_example.module for more details on the
      // properties of #ajax.
      // '#ajax' => [
      // 'callback' => '::insightlyAddMoreCallback',
      // 'wrapper' => 'insightly-fieldset-wrapper',
      // ],.
    ];
    $form['insightly_submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
      '#validate' => ['::insightlyAddFormValidate'],
      '#submit' => ['::insightlyAddFormSubmit'],
    ];
    return $form;
  }

  /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller. It must be
   * unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'insightly_form';
  }

  /**
   * Implements form validation.
   *
   * The validateForm method is the default method called to validate input on
   * a form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Implements a form submit handler.
   *
   * The submitForm method is the default method called for any submit elements.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the mapping in it.
   */
  public function webformInsightlyEditAddMoreCallback($form, $form_state) {
    return $form['insightly_mapping_edit_fieldset'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function insightlyAddMoreFields($form, &$form_state) {
    $num_fields = $form_state->get('num_fields');
    $num_fields++;
    $form_state->set('num_fields', $num_fields);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the mapping in it.
   */
  public function insightlyAddMoreCallback($form, $form_state) {
    return $form['insightly_mapping_fieldset'];
  }

  /**
   * Function to validate insightly_add_form.
   */
  public function insightlyAddFormValidate($form, &$form_state) {
    $form_id = $form_state->getValue('wrapper_fieldset')['insightly_form_id'];
    $results = array();
    $query = \Drupal::database()->select('insightly', 'i');
    $query->fields('i', ['form_id']);
    $query->condition('form_id', $form_id);
    $rows = $query->execute();
    while ($row = $rows->fetchAssoc()) {
      $results[] = $row;
    }
    foreach ($results as $result) {
      $form_state->setErrorByName('insightly_form_id', t('The entered form is already mapped!'));
    }
  }

  /**
   * Submit handler to save the mapping configuration as JSON format in db.
   */
  public function insightlyAddFormSubmit($form, &$form_state) {
    $config = \Drupal::service('config.factory')->getEditable('insightly.settings');
    $value = $form_state->getValue('insightly_api_key');
    $config->set('insightly_api_key', $value)->save();
    $form_id = $form_state->getValue('wrapper_fieldset')['insightly_form_id'];
    if (!$form_id) {
      $messenger = \Drupal::messenger();
      $messenger->addWarning($this->t("No Mapping details has been added! API key has been saved."));
      return TRUE;
    }
    $field_set_values = $form_state->getValue('wrapper_fieldset')['insightly_mapping_fieldset'];
    foreach ($field_set_values as $value) {
      if ($value['insightly_remove'] == 0) {
        if (trim($value['insightly_source']) != NULL) {
          $mapping[$value['insightly_source']] = $value['insightly_target'];
        }
      }
    }
    $mapping = json_encode($mapping);
    $entry = [
      'form_id' => $form_id,
      'mapping' => $mapping,
    ];
    $insert = \Drupal::database()->insert('insightly');
    $insert->fields([
      'form_id',
      'mapping',
    ]);
    $results = $insert->values(array(
      $form_id,
      $mapping,
    ))->execute();

    // Save mapped forms in insightly settings.
    $config = \Drupal::service('config.factory')->getEditable('insightly.settings');
    $mapped_forms = $config->get('insightly_mapped_forms');
    // For the inital time, $mapped_forms will be null.
    if ($mapped_forms == NULL) {
      // Create an array for storing mapped forms.
      $mapped_forms = [];
    }
    $mapped_forms[] = $form_id;
    $config->set('insightly_mapped_forms', $mapped_forms)->save();
  }

}
