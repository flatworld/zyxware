<?php

namespace Drupal\insightly\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the SimpleForm form controller.
 *
 * This example demonstrates a simple form with a singe text input element. We
 * extend FormBase which is the simplest form base class used in Drupal.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class ExportForm extends FormBase {

  /**
   * Build the simple form.
   *
   * A build form method constructs an array that defines how markup and
   * other form elements are included in an HTML form.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {

    $form_state->set('insightly_table_form_id', $id);
    $mapping_details = $this->insightlyGetFieldMapping($id);
    $form['insightly_export'] = [
      '#title' => t('Export'),
      '#type' => 'textarea',
      '#default_value' => $mapping_details['form_id'] . '|' . $mapping_details['mapping'],
    ];

    return $form;
  }

  /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller. It must be
   * unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'insightly_form';
  }

  /**
   * Implements form validation.
   *
   * The validateForm method is the default method called to validate input on
   * a form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Implements a form submit handler.
   *
   * The submitForm method is the default method called for any submit elements.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Helper functions to get mapping details for corresponding id.
   *
   * @param string $id
   *   Unique id for mapping details entry.
   */
  public function insightlyGetFieldMapping($id) {

    $query = \Drupal::database()->select('insightly', 'i');
    $query->fields('i', ['id', 'form_id', 'mapping']);
    $query->condition('id', $id);
    $results = $query->execute();
    while ($row = $results->fetchAssoc()) {
      $mapping_details['form_id'] = $row['form_id'];
      $mapping_details['mapping'] = $row['mapping'];
    }
    return $mapping_details;
  }

}
