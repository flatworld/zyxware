<?php

namespace Drupal\insightly\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;

/**
 * Implements the SimpleForm form controller.
 *
 * This example demonstrates a simple form with a singe text input element. We
 * extend FormBase which is the simplest form base class used in Drupal.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class EditForm extends FormBase {

  /**
   * Build the edit form to edit mapped form data.
   *
   * A build form method constructs an array that defines how markup and
   * other form elements are included in an HTML form.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {

    $config = \Drupal::service('config.factory')->getEditable('insightly.settings');
    $form['#tree'] = TRUE;
    $form_state->set('form_table_id', $id);
    $num_fields = $form_state->get('num_fields');
    if (empty($num_fields)) {
      $form_state->set('num_fields', 0);
    }
    $mapping_details = $this->insightlyGetFieldMapping($id);
    // Save form id for the updation in insightly.settings.
    $_SESSION['form_id'] = $mapping_details['form_id'];
    $mapping_details['mapping'] = json_decode($mapping_details['mapping'], TRUE);
    // If get wrong id then goto page not found.
    if (empty($mapping_details)) {
      $response = new RedirectResponse(\Drupal::url('<front>'));
      $response->send();
    }
    if ($mapping_details) {
      $form['insightly_edit_form_id'] = [
        '#type' => 'textfield',
        '#title' => t('Form id'),
        '#required' => TRUE,
        '#default_value' => $mapping_details['form_id'],
      ];
      $accepted_fields = $config->get('insightly_accepted_fields');
      $field_list_string = '';
      foreach ($accepted_fields as $accepted_field) {
        $field_list_string .= $accepted_field . ', ';
      }
      $field_list_string = substr_replace(trim($field_list_string), "", -1);
      $text = 'Available fields in insightly to set target values are :field_list_string All';
      $text .= 'other data field will be added as CUSTOMFIELDS (You need to add custom fields ';
      $text .= 'at System Settings > Custom Fields).';
      $form['insightly_fields_markup'] = [
        '#markup' => t($text, [':field_list_string' => $field_list_string . '.']),
        '#prefix' => '<div class="clear">',
        '#suffix' => '</div>',
      ];
      $form['insightly_mapping_edit_fieldset'] = [
        '#type' => 'fieldset',
        '#title' => t('Mapping'),
        // Set up the wrapper so that AJAX will be able to replace the fieldset.
        '#prefix' => '<div id="mapping-fieldset-wrapper">',
        '#suffix' => '</div>',
      ];
      $count = 0;
      foreach ($mapping_details['mapping'] as $source => $target) {
        $form['insightly_mapping_edit_fieldset'][$count]['source'] = [
          '#type' => 'textfield',
          '#title' => t('Source'),
          '#default_value' => $source,
          '#prefix' => '<div class="col-source">',
          '#suffix' => '</div>',
        ];
        $form['insightly_mapping_edit_fieldset'][$count]['target'] = [
          '#type' => 'textfield',
          '#title' => t('Target'),
          '#default_value' => $target,
          '#prefix' => '<div class="col-target">',
          '#suffix' => '</div>',
        ];
        $form['insightly_mapping_edit_fieldset'][$count]['remove'] = [
          '#title' => t('Remove'),
          '#type' => 'checkbox',
          '#default_value' => 0,
          '#prefix' => '<div class="col-remove">',
          '#suffix' => '</div>',
        ];
        $count++;
      }
    }
    $loop_condition_limit = $form_state->get('num_fields');
    for ($i = $count; $i < $loop_condition_limit + $count; $i++) {
      $form['insightly_mapping_edit_fieldset'][$i]['source'] = [
        '#type' => 'textfield',
        '#title' => t('Source'),
        '#prefix' => '<div class="col-source">',
        '#suffix' => '</div>',
      ];
      $form['insightly_mapping_edit_fieldset'][$i]['target'] = [
        '#type' => 'textfield',
        '#title' => t('Target'),
        '#prefix' => '<div class="col-target">',
        '#suffix' => '</div>',
      ];
      $form['insightly_mapping_edit_fieldset'][$i]['remove'] = [
        '#title' => t('Remove'),
        '#type' => 'checkbox',
        '#default_value' => 0,
        '#prefix' => '<div class="col-remove">',
        '#suffix' => '</div>',
      ];
    }
    $form['insightly_add_field'] = [
      '#type' => 'submit',
      '#value' => t('Add Field'),
      '#submit' => ['::insightlyAddMoreFields'],
      // See the examples in ajax_example.module for more details on the
      // properties of #ajax.
      /*
      '#ajax' => [
        'callback' => 'webform_insightly_edit_add_more_callback',
        'wrapper' => 'mapping-fieldset-wrapper',
      ],
      */
    ];
    $form['insightly_edit_submit'] = [
      '#type' => 'submit',
      '#value' => t('Update'),
      '#submit' => ['::insightlyEditFormSubmit'],
    ];
    return $form;
  }

  /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller. It must be
   * unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'insightly_form';
  }

  /**
   * Implements form validation.
   *
   * The validateForm method is the default method called to validate input on
   * a form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Implements a form submit handler.
   *
   * The submitForm method is the default method called for any submit elements.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Helper functions to get mapping details for corresponding id.
   *
   * @param string $id
   *   Unique id for mapping details entry.
   */
  public function insightlyGetFieldMapping($id) {
    $query = \Drupal::database()->select('insightly', 'i');
    $query->fields('i', ['id', 'form_id', 'mapping']);
    $query->condition('id', $id);
    $results = $query->execute();
    while ($row = $results->fetchAssoc()) {
      $mapping_details['form_id'] = $row['form_id'];
      $mapping_details['mapping'] = $row['mapping'];
    }
    return $mapping_details;
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function insightlyAddMoreFields($form, &$form_state) {
    $num_fields = $form_state->get('num_fields');
    $num_fields++;
    $form_state->set('num_fields', $num_fields);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Submit handler to update the mapping configuration.
   */
  public function insightlyEditFormSubmit($form, &$form_state) {
    $id = $form_state->get('form_table_id');
    $form_id = $form_state->getValue('insightly_edit_form_id');
    $form_values = $form_state->getValue('insightly_mapping_edit_fieldset');
    foreach ($form_values as $value) {
      if ($value['remove'] == 0) {
        if (trim($value['source']) != NULL) {
          $mapping[$value['source']] = $value['target'];
        }
      }
    }
    $mapping = json_encode($mapping);
    // Updating the fields in the db.
    $update = \Drupal::database()->update('insightly');
    $update->condition('id', $id);
    $update->fields([
      'id' => $id,
      'form_id' => $form_id,
      'mapping' => $mapping,
    ]);
    $update->execute();

    // Update the form id in insightly.settings.
    $config = \Drupal::service('config.factory')->getEditable('insightly.settings');
    $mapped_forms = $config->get('insightly_mapped_forms');
    $index = array_search($_SESSION['form_id'], $mapped_forms);
    $mapped_forms[$index] = $form_id;
    $config->set('insightly_mapped_forms', $mapped_forms)->save();

    // Redirecting to admin/config/insightly-mapping/add.
    $response = new RedirectResponse(\Drupal::url('insightly.add'));
    $response->send();
  }

}
