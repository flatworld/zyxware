<?php

namespace Drupal\user_batch_process\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class UpdateUserDescForm.
 *
 * @package Drupal\user_batch_process\Form
 */
class UpdateUserDescForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'update_user_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['update_user'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Update User'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $query = \Drupal::entityQuery('user')
      ->condition('field_user_description', NULL, 'IS NOT NULL');
      // ->sort('created', 'ASC')
      // ->range(0,200);
      $uids = $query->execute();  

    $batch = array(
      'title' => t('Updating User...'),
      'operations' => array(
        array(
          '\Drupal\user_batch_process\UpdateUser::updateUserBatch',
          array($uids)
        ),
      ),
      'finished' => '\Drupal\user_batch_process\UpdateUser::deleteNodeExampleFinishedCallback',
    );

    batch_set($batch);
  }

}