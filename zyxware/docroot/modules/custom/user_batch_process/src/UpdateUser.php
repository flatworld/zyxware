<?php

namespace Drupal\user_batch_process;


use Drupal\user\Entity\User;

class UpdateUser {

  public static function updateUserBatch($uids, &$context){
    $message = 'updating user...';
    $results = array();
    foreach ($uids as $uid) {
        $user = User::load($uid);
        $value = $user->get('field_user_description')->value;
        $user->field_user_description2->setValue(['value' => $value, 'format' => 'full_html']);
        $results[] = $user->save();
    }
    $context['message'] = $message;
    $context['results'] = $results;
  }

  function updateUserBatchFinishedCallback($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One post processed.', '@count posts processed.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    // drupal_set_message($message);
  }
}