<?php

namespace Drupal\landing_page\Plugin\Filter;

use Drupal\block\Entity\Block;
use Drupal\block_content\Entity\BlockContent;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Filter for landing page.
 *
 * @Filter(
 *   id = "filter_landingpage",
 *   title = @Translation("Landing Page Filter"),
 *   description = @Translation("New text format created for landing page module."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class FilterLandingpage extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    preg_match_all('/\\[([a-zA-z0-9_\-]+:[a-zA-z0-9_\-]+)+\\]/m', $text, $match);
    $matches = $match[0];
    foreach ($matches as $content_token) {
      $content_token_name = str_replace(['[', ']'], '', $content_token);
      $type = explode(':', $content_token_name);

      // For system blocks, view blocks, custom blocks.
      // format [block:block_id].
      if ($type[0] == 'block') {
        $bid = trim($type[1]);
        if (is_numeric($bid)) {
          $block = BlockContent::load($bid);
          $block_content = \Drupal::entityTypeManager()->getViewBuilder('block_content')->view($block);
          $replacement = \Drupal::service('renderer')->render($block_content);
        }
        elseif (!is_numeric($bid)) {
          $block = Block::load($bid);
          $block_content = \Drupal::entityManager()->getViewBuilder('block')->view($block);
          $replacement = \Drupal::service('renderer')->render($block_content);
        }

      }
      // For contact forms
      // Format [form:form_id].
      if ($type[0] == 'form') {
        $form_id = $type[1];
        $message = \Drupal::entityTypeManager()
          ->getStorage('contact_message')
          ->create([
            'contact_form' => $form_id,
          ]);
        $form = \Drupal::service('entity.form_builder')->getForm($message);
        $replacement = \Drupal::service('renderer')->renderRoot($form);
      }
      // Change html only for [form:form_id] and [block:block_id].
      if ($type[0] == 'form' || $type[0] == 'block') {
        $text = str_replace($content_token, $replacement, $text);
      }
    }
    return new FilterProcessResult($text);
  }

}
