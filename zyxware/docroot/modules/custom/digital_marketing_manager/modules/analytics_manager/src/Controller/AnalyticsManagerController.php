<?php

namespace Drupal\analytics_manager\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Database\Database;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\node\Entity\Node;

/**
 * Controller routines for analytics manager routes.
 */
class AnalyticsManagerController extends ControllerBase {

  /**
   * Function that returns the submitted internal links.
   */
  public function getInternalLinks() {
    $header = [
      'Path' => t('Path'),
      'Tag' => t('Tag'),
      'Edit' => t('Edit'),
      'Delete' => t('Delete'),
    ];
    $query = \Drupal::database()->select('am_internal_links', 'ami');
    $query->fields('ami');
    $results = $query->execute()->fetchAll();
    foreach ($results as $row) {
      $rowid            = $row->id;
      $delete           = Url::fromUserInput('/admin/config/analytics_manager/delete_internal_links/' . $rowid);
      $edit             = Url::fromUserInput('/admin/config/analytics_manager/edit_internal_links/' . $rowid);
      $output[$row->id] = [
        'Path' => $row->path,
        'Tag' => $row->classification,
        \Drupal::l('Edit', $edit),
        \Drupal::l('Delete', $delete),
      ];
    }
    $form['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $output,
      '#empty' => t('No results found'),
    ];
    return $form;
  }

  /**
   * Function that delete internal links.
   */
  public function deleteInternalLinks($id = NULL) {
    $query = \Drupal::database()->delete('am_internal_links');
    $query->condition('id', $id);
    $deleted = $query->execute();
    if ($deleted == TRUE) {
      \Drupal::messenger()->addMessage($this->t('Successfully Deleted!'));
    }
    else {
      \Drupal::messenger()->addMessage($this->t('Error!'), 'error');
    }
    return new RedirectResponse(\Drupal::url('analytics_manager.internal_links_view'));
  }

  /**
   * Function to return Page Type or Tag of corresponding url from "internal_links" table.
   */
  public function analytics_manager_get_tag_from_table($url) {
    $query = \Drupal::database()->select('am_internal_links', 'ami')
      ->fields('ami')
      ->condition('path', $url)
      ->execute()
      ->fetchAssoc();
    if (!empty($query)) {
      return $query['classification'];
    }
    else {
      return FALSE;
    }
  }

  /**
   * Function that returns the Page Type or Tag.
   */
  public function analytics_manager_get_page_tag() {
    $current_path = \Drupal::service('path.current')->getPath();
    $arg = explode('/', $current_path);
    $nid = $arg['2'];
    if ($arg['1'] == 'node' && is_numeric($nid)) {
      $node = Node::load($nid);
      $type_name = $node->bundle();
      if ($type_name == 'story' || $type_name == 'page') {
        $field_page_type = $node->field_page_type->getValue()['0']['value'];
        if ($field_page_type) {
          $tag = $field_page_type;
        }
        elseif (empty($field_page_type)) {
          $url = \Drupal::service('path.alias_manager')->getAliasByPath($current_path);
          $tag = $this->analytics_manager_get_tag_from_table($url);
          if ($tag == FALSE) {
            $tag = 'Other';
          }
        }
        // Else, Token is set in google analytics configuration.
      }
      elseif ($type_name == 'fs_freesoftware' || $type_name == 'product'
        || $type_name == 'product_kit' || $type_name == 'ram'
        || $type_name == 'pen_drive' || $type_name == 'hard_disk') {
        $tag = 'Store';
      }
      elseif ($type_name == "landing_page") {
        $tag = 'LandingPage';
      }
      elseif ($type_name == 'services') {
        $tag = 'Marketing';
      }
      else {
        $tag = 'Other';
      }
    }
    elseif ($arg['1'] == 'search') {
      $tag = 'Search';
    }
    elseif ($arg['1'] == 'steward') {
      $tag = 'About';
    }
    elseif ($arg['1'] == 'requestcd') {
      $tag = 'Store';
    }
    else {
      $url = \Drupal::service('path.alias_manager')->getAliasByPath($current_path);
      $tag = $this->analytics_manager_get_tag_from_table($url);
      if ($tag == FALSE) {
        $tag = 'Other';
      }
    }
    return $tag;
  }

}
