<?php

namespace Drupal\analytics_manager\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for allowed content types in a Group.
 */
class GetContentTypesForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'get_content_types_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'analytics_manager.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('analytics_manager.settings');
    $options = $this->getContentTypes();
    $dis = [];
    $default_values = [];
    foreach ($options as $key => $content_type) {
      $bundle_fields = \Drupal::entityManager()->getFieldDefinitions('node', $key);
      if (array_key_exists('field_page_type', $bundle_fields)) {
        $dis[$key] = [
          '#disabled' => TRUE,
        ];
        $default_values[$key] = $key;
      }
    }
    $form['content_types'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#title' => 'Content Types',
      '#description' => 'Select the bundles requires page type needs to be assigned. <br />
        <strong>NOTE: Once added Page Type field cannot be removed from those bundles.</strong>',
      '#multiple' => TRUE,
      '#default_value' => $default_values,
    ];
    $form['content_types'] = array_merge($form['content_types'], $dis);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $content_types = array_filter($form_state->getValue('content_types'));
    if (empty($content_types)) {
      $form_state->setErrorByName('content_types', $this->t('Select any content type'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('analytics_manager.settings');
    $config->set('content_types', $form_state->getValue('content_types'));
    $config->save();
    $content_types = array_filter($form_state->getValue('content_types'));
    foreach ($content_types as $key => $contentType) {
      $bundle_fields = \Drupal::entityManager()->getFieldDefinitions('node', $key);
      if (!array_key_exists('field_page_type', $bundle_fields)) {
        analytics_manager_fields('node', $key);
      }
    }
    return parent::submitForm($form, $form_state);
  }

  /**
   * Function to get all content types.
   */
  public function getContentTypes() {
    $contentTypes = \Drupal::service('entity.manager')->getStorage('node_type')->loadMultiple();
    $contentTypesList = [];
    foreach ($contentTypes as $contentType) {
      $contentTypesList[$contentType->id()] = $contentType->label();
    }
    return $contentTypesList;
  }

}
