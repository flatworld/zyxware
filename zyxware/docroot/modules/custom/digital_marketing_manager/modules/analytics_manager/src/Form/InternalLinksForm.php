<?php

namespace Drupal\analytics_manager\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;

/**
 * Defines a form that configures forms module settings.
 */
class InternalLinksForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'internal_links_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['path'] = [
      '#title' => t('Path'),
      '#type' => 'textfield',
      '#size' => 100,
      '#required' => TRUE,
      '#description' => t('Enter the url or url pattern to which GA tags is to be assigned.'),
    ];
    $form['tag'] = [
      '#title' => t('Page Type'),
      '#type' => 'textfield',
      '#size' => 60,
      '#required' => TRUE,
      '#description' => t('Enter the Page Type or tag to be associated with the url.'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $path = $form_state->getValue('path');
    $query = \Drupal::database()->select('am_internal_links', 'ami')
      ->fields('ami')
      ->condition('path', $path)
      ->execute()
      ->fetchAssoc();

    if ($query) {
      $form_state->setErrorByName('path', $this->t('This path has already been entered. Please delete the record or update it.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $path = $form_state->getValue('path');
    $tag = $form_state->getValue('tag');
    \Drupal::database()->insert('am_internal_links')
      ->fields([
        'path' => $path,
        'classification' => $tag,
      ])
      ->execute();
    \Drupal::messenger()->addMessage($this->t('Successfully Submitted'));
    $form_state->setRedirect('analytics_manager.internal_links_view');
  }

}
