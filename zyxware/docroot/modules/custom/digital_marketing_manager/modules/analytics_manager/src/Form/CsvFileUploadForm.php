<?php

namespace Drupal\analytics_manager\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Database\Database;

/**
 * Defines a form that configures forms module settings.
 */
class CsvFileUploadForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'csv_file_upload_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['csv_file'] = [
      '#type' => 'managed_file',
      '#title' => t('Upload csv file with url and tag (path, page type)'),
      '#required' => TRUE,
      '#upload_location'    => 'public://',
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $file = \Drupal::entityTypeManager()->getStorage('file')->load($form_state->getValue('csv_file')[0]);
    $fid = $file->get('fid')->value;
    $handle = fopen($file->get('uri')->value, 'r');
    $already_existing = '';
    $count = 0;
    while ($row = fgetcsv($handle)) {
      if ($count != 0) {
        $query = \Drupal::database()->select('am_internal_links', 'ami')
          ->fields('ami')
          ->condition('path', $row[0])
          ->execute()
          ->fetchAssoc();

        if (empty($query)) {
          \Drupal::database()->insert('am_internal_links')
            ->fields([
              'path' => $row[0],
              'classification' => $row[1],
            ])
            ->execute();
        }
        elseif (!empty($query)) {
          $already_existing .= $row[0] . '-' . $row[1] . "<br>";
        }
      }
      $count++;
    }
    if (!empty($already_existing)) {
      \Drupal::messenger()->addMessage($this->t($already_existing . 'These paths has already been entered. Please delete the record or update it.'));
    }
    file_delete($fid);
    $url = Url::fromRoute('analytics_manager.internal_links_view');
    $form_state->setRedirectUrl($url);
  }

}
