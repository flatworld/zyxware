<?php

namespace Drupal\lead_generation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class SumomeConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lead_generation_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'lead_generation.settings',
    ];
  }

  /**
   * Form to collect sumome details.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('lead_generation.settings');
    $form['sumome_site_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sumo Site ID'),
      '#required' => TRUE,
      '#title' => t('Sumo Site ID'),
      '#description' => t('Enter the entire string of numbers and letters after data-sumo-site-id= in the HTML Code you
          were givin on the SumoMe.com website. Do not include the beginning and ending quotes.'),
      '#default_value' => $config->get('sumome_site_id'),
    ];
    $form['sumome_content_channels'] = [
      '#prefix' => '<h3>Content Channels to Enable Sumome</h3>',
      '#type' => 'textarea',
      '#required' => FALSE,
      '#title' => t('Sumome content channels'),
      '#description' => t('Enter the content channels separated by a comma.'),
      '#default_value' => $config->get('sumome_content_channels'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('lead_generation.settings');
    $config->set('sumome_site_id', $form_state->getValue('sumome_site_id'));
    $config->set('sumome_content_channels', $form_state->getValue('sumome_content_channels'));
    $config->save();

    return parent::submitForm($form, $form_state);
  }

}
