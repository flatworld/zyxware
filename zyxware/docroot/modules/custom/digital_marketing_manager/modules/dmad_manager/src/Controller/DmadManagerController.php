<?php

namespace Drupal\dmad_manager\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller routines for reseller routes.
 */
class DmadManagerController extends ControllerBase {

  /**
   * Function to run campaigns.
   */
  public function runCampaigns() {

    list($array, $urls) = $this->dirToArray('campaigns/');
    // [Channel][Medium][Campaign].
    $ch_me_ca = [];
    // [Campaign][Medium][Variant].
    $ca_me_va = [];
    // Urls of all the images.
    $paths = [];
    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('dmad_manager')->getPath();
    $file = $module_path . '/js/dmad_variables.js';

    // Parse the array and create necessary js arrays.
    foreach ($array as $channel => $campaigns) {
      foreach ($campaigns as $campaign => $regions) {
        foreach ($regions as $region => $variants) {
          if (count($campaigns) == 1) {
            $ch_me_ca[$channel][$region][$campaign] = 10;
          }
          else {
            // Getting the weights of the campaign.
            $explode = explode('-', $campaign);
            $ch_me_ca[$channel][$region][$campaign] = (int) end($explode);
          }
          foreach ($variants as $variant) {
            $file_name = explode('.', $variant);
            // Set link for corresponding images from the text file.
            if ($file_name[1] == 'txt') {
              $path = $module_path . '/' . $urls[$variant];
              $call_action = file_get_contents($path);
              $ca_me_va[$campaign][$region][$file_name[0]]['call_action'][$variant] = $call_action;
            }
            if (count($variants) == 2) {
              if ($file_name[1] != 'txt') {
                $ca_me_va[$campaign][$region][$file_name[0]]['image'][$variant] = 10;
              }
            }
            else {
              // Getting the weights of the ad variants.
              $explode = explode('-', $variant);
              $percentage = (int) explode('.', end($explode))[0];
              if ($file_name[1] != 'txt') {
                $ca_me_va[$campaign][$region][$file_name[0]]['image'][$variant] = $percentage;
              }
            }
          }
        }
      }
    }

    // Remove text file links.
    foreach ($urls as $key => $value) {
      $file_name = explode('.', $key);
      if ($file_name[1] == 'txt') {
        unset($urls[$key]);
      }
    }

    $all_campaigns = json_encode($ch_me_ca);
    $all_variants = json_encode($ca_me_va);
    $all_urls = json_encode($urls);
    $script = "var campaigns = $all_campaigns;"
          . "var variants = $all_variants;"
          . "var urls = $all_urls";
    // Write the contents back to the file.
    file_put_contents($file, $script);

    return [];
  }

  /**
   * Function that parses the folder structure and creates arrays with the ad campaign details.
   *
   * @param string $dir
   *
   * @return array
   */
  public function dirToArray($dir) {
    $arr = [];
    $image_urls = [];
    $mask = '/\.(jpe?g|png|gif|bmp|TXT|txt)$/i';
    $array_campaigns = array_reverse(file_scan_directory(DRUPAL_ROOT . '/modules/custom/digital_marketing_manager/modules/dmad_manager/campaigns', $mask, $options = []));
    foreach ($array_campaigns as $data) {
      $count_path = count(explode('/', $data->uri));
      $pwd = explode('/', $data->uri, ($count_path - 4));
      $image_urls[$data->filename] = $pwd[$count_path - 5];
      $pwd2 = explode('/', $data->uri);
      $arr[$pwd2[$count_path - 4]][$pwd2[$count_path - 3]][$pwd2[$count_path - 2]][] = $pwd2[$count_path - 1];
    }
    return [$arr, $image_urls];
  }

}
