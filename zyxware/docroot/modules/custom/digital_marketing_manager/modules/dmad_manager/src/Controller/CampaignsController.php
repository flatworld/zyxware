<?php

namespace Drupal\dmad_manager\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Controller routines for reseller routes.
 */
class CampaignsController extends ControllerBase {

  /**
   * Function to create a link to run campaigns.
   */
  public function scanCampaigns() {
    $url = Url::fromRoute('dmad_manager.campaigns')->toString();
    $internal_link = 'hell';
    $markup = "<h1>" . t('Add Campaigns') . "</h1></br>";
    $markup .= '<a href="' . $url . '">' . t('Add Campaigns') . '</a>';
    $build = [
      '#type' => 'markup',
      '#markup' => $markup,
    ];
    return $build;
  }

}
