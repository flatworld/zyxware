<?php

namespace Drupal\zyxware_custom\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Default controller for the Zyxware Custom module.
 */
class ZyxwareCustomController extends ControllerBase {

  /**
   * Creates a home page.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function home() {
    return ['#markup' => ''];
  }

}
