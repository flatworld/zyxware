<?php

namespace Drupal\zyxware_custom\Plugin\Block;

use Drupal\field\Entity\FieldConfig;
use Drupal\user\Entity\User;
use Drupal\Component\Utility\Html;
use Drupal\Core\Block\BlockBase;
use Drupal\image\Entity\ImageStyle;

/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "Node_Header",
 *   admin_label = @Translation("Node Header"),
 *   category = @Translation("Node Header"),
 * )
 */
class NodeHeader extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get the current node.
    $node = \Drupal::routeMatch()->getParameter('node');
    $shares = 0;
    $terms = [];
    if ($node) {
      // Get the user id.
      $usid = $node->getOwner();
      $userid = $node->getOwnerId();
      $nodetitle = $node->getTitle();
      $user = User::load($userid);
      if ($user) {
        $picture = "";
        if (isset($user->get('user_picture')->entity)) {
          $url = $user->get('user_picture')->entity->url();
          $uri = $user->get('user_picture')->entity->getFileUri();
          if ($uri) {
            $picture = ImageStyle::load('user_image')->buildUrl($uri);
          }
        }
        else {
          $field = FieldConfig::loadByName('user', 'user', 'user_picture');
          $default_image = $field->getSetting('default_image');
          $file = \Drupal::entityManager()->loadEntityByUuid('file', $default_image['uuid']);
          $uri = $file->getFileUri();
          if ($uri) {
            $picture = ImageStyle::load('user_image')->buildUrl($uri);
          }
        }
        // Get the username from the id.
        $username = $usid->getDisplayName();
        // Get the node created time.
        $created_time = $node->getCreatedTime();
        $time = $this->timeAgo('@' . $created_time);
        // Checking whether the node is of content type is article.
        $definitions = \Drupal::service('entity_field.manager')->getFieldMapByFieldType('comment');
        $commenttype = $definitions['node'];
        foreach ($commenttype as $k => $com) {
          if (isset($com['bundles']) && in_array($node->getType(), $com['bundles'])) {
            $key = $k;
          }
        }

        if ($node->hasField($key)) {
          // Get the comment count of node.
          $comment_count = $node->get($key)->comment_count;
        }
        if ($node->hasField('field_tags')) {
          // Getting the taxonomy terms related to the node.
          $referenced_entities = $node->get('field_tags')->referencedEntities();
          foreach ($referenced_entities as $term) {
            $terms[] = Html::cleanCssIdentifier($term->getName());
          }
        }
        if ($node->hasField('taxonomy_vocabulary_2')) {
          $terms = [];
          // Getting the taxonomy terms related to the node.
          $referenced_entities = $node->get('taxonomy_vocabulary_2')->referencedEntities();
          foreach ($referenced_entities as $term) {
            $terms[] = Html::cleanCssIdentifier($term->getName());
          }
        }
      }
      $profile_link = '<a href="/user/'.$userid.'">'.$username.'</a>';
      $output = '';
      $output .= '<div class="node_title_rendered">' . $nodetitle . '</div>';
      $output .= '<div class="node_header">';

      $output .= '<div class="userimg"><img class="circle personpic responsive-img" src=' . $picture . ' alt=' . $picture . '> <div class="username"> ' . t('BY ') . $profile_link . ' <div class="time">' . $time . '</div></div></div>';

      if (isset($terms[0])) {
        $output .= '<div class="tags"><div class="tag">' . $terms[0] . '</div></div>';
      }
      if ($node->getType() == "case_studies") {

      }

      else {
        $output .= '<div class="comments"><div class="comment">' . $comment_count . ' comments <i class="small material-icons">comment</i></div></div>';
      }

      $output .= '<div class="shares"><div class="share">' . $shares . ' shares <i class="small material-icons">shares</i></div></div>';

      if ($node->getType() == "case_studies") {
        // Include Download button.
        $output .= '<div class="Download-case-study custom-download-case-study"><div class="case-studies"><a class="zyxpro-download-casestudy">' . 'DOWNLOAD' . '</a></div></div>';

        // Include talk to an expert button.
        $output .= '<div class="talk-to-an-expert"><a class="getin-touch-btn" href="/contact-us">';
        $output .= 'Talk to an expert</a></div>';
      }

      $output .= '</div>';

      return ['#markup' => $output];
    }
  }

  /**
   *
   */
  public function timeAgo($datetime, $full = FALSE) {
    $now = new \DateTime();
    $ago = new \DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = [
      'y' => 'year',
      'm' => 'month',
      'w' => 'week',
      'd' => 'day',
      'h' => 'hour',
      'i' => 'minute',
      's' => 'second',
    ];
    foreach ($string as $k => &$v) {
      if ($diff->$k) {
        $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
      }
      else {
        unset($string[$k]);
      }
    }

    if (!$full) {
      $string = array_slice($string, 0, 1);
    }
    return $string ? implode(', ', $string) . ' ago' : 'just now';
  }

}
