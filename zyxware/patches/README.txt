README.txt

Couldn't apply menu_ui.patch for the latest Drupal core update(8.6.10). If the error related to invalid assignment opertaion on menu you might need to mdodify and apply the following lines as a patch for menu_ui.module file.

diff --git a/docroot/core/modules/menu_ui/menu_ui.module b/docroot/core/modules/menu_ui/menu_ui.module
index 7e37cafbd..edaa3f642 100644
--- a/docroot/core/modules/menu_ui/menu_ui.module
+++ b/docroot/core/modules/menu_ui/menu_ui.module
@@ -229,7 +229,7 @@ function menu_ui_form_node_form_alter(&$form, FormStateInterface $form_state) {
   $type_menus = Menu::loadMultiple($type_menus_ids);
   $available_menus = [];
   foreach ($type_menus as $menu) {
-    $available_menus[$menu->id()] = $menu->label();
+    $available_menus[$menu->id()] = $menu->label() ?? '';
   }
   if ($defaults['id']) {
     $default = $defaults['menu_name'] . ':' . $defaults['parent'];
